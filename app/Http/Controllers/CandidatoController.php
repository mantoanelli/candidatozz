<?php

namespace App\Http\Controllers;

use App\Candidato;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CandidatoController extends Controller {

    public function createCandidato(Request $request) {
       
        $candidato = Candidato::create($request->all());
        return response()->json($candidato);
    }

    public function updateCandidato(Request $request, $id) {

        $candidato = Candidato::find($id);
        $candidato->nome = $request->input('nome');
        $candidato->email = $request->input('email');
        
        $candidato->sexo = $request->input('sexo');
        $candidato->save();

        return response()->json($candidato);
    }

    public function deleteCandidato($id) {
        $candidato = Candidato::find($id);
        $candidato->delete();

        return response()->json('Removido com sucesso.');
    }
    

    public function index($id=null) {

        if($id){
            $candidatos = Candidato::find($id);
        }else{
            $candidatos = Candidato::all();
        }
        return response()->json($candidatos);
    }

}
