<?php
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Candidato extends Model
{ 
 	protected $fillable = ['nome', 'email', 'data_nascimento','sexo'];	 
}
