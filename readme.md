# Projeto de teste Candidatozz

Projeto dedicado para teste na empresa Eduzz.

# Observações
O front foi desenvolvido na pasta "front". O Dump do sql está na pasta "sql".

# Agradecimento

Queria agradecer desde já a oportunidade de poder conhecer a empresa Eduzz e o framework Lumen, é meu primeiro contato com ele e vi que é bem ágil para liberar APIs.
Espero ter atingido os objetivos da tarefa.

# Rogerio Mantoanelli
http://www.rogeriomaster.com.br

https://www.linkedin.com/in/rogeriomantoanelli/

rogeriofotovibe@gmail.com

