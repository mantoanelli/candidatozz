$(function () {

    carregaLista();


    $('.inputDate').removeClass('hasDatepicker');
    $('.inputDate').removeAttr('id');
    $('.inputDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true
    }, 'option', {
        dateFormat: 'dd/mm/yy'
    });

});

function carregaLista() {
    $.ajax({
        url: '../public/candidato',
        headers: {"Content-type": 'application/json'},
        type: 'GET',
        success: function (data) {
            atualizaLista(data);
        }
    });
}

function atualizaLista(data) {
    $('#lista tbody').html('');
    $.each(data, function (k, v) {
        _html = '<tr idRegistro="' + v.id + '">\
            <th scope="row" class="">' + v.id + '</th> \
            <td>' + v.nome + '</td>\
            <td>' + v.email + '</td>\
            <td>' + dataBr(v.data_nascimento) + '</td>\
            <td>' + v.sexo + '</td>\
            <td>\
                <div class="btn-group">\
                    <button type="button" class="btn btn-primary edit"><span class="glyphicon glyphicon-edit"></span></button>\
                    <button type="button" class="btn btn-danger delete"><span class="glyphicon glyphicon-trash"></span></button>\
                </div>\
            </td>\
        </tr>';

        $('#lista tbody').append(_html);
    });

    bindsActions();
}

function bindsActions() {
    $('#lista tbody button.delete').unbind('click');
    $('#lista tbody button.edit').unbind('click');
    $('#lista tbody button.delete').bind('click', listaEventButtons);
    $('#lista tbody button.edit').bind('click', openFormCandidatoEdit);

    $('#addCandidato').unbind('click');
    $('#addCandidato').bind('click', openFormCandidato);
}

function openFormCandidato() {
    _form = $('#novo');
    _form.find('input[type="text"]').val('');
    _form.find('input[type="hidden"]').val('');
    _form.find('input').removeAttr('readonly');
    _form.modal('show');
    _form.find('button.salvar').unbind('click');
    _form.find('button.salvar').bind('click', salvarCandidato);
}

function openFormCandidatoEdit() {
    o = $(this);
    _form = $('#novo');
    _form.find('input[type="text"]').val('');
    _form.find('input[type="hidden"]').val(o.parents('tr').attr('idRegistro'));
    _form.find('input').removeAttr('readonly');

    $.ajax({
        url: '../public/candidato/' + o.parents('tr').attr('idRegistro'),
        headers: {"Content-type": 'application/json'},
        type: 'GET',
        success: function (data) {
            _campos = ['nome', 'email', 'data_nascimento', 'sexo'];
            $.each(_campos, function (k, v) {
                if (v == 'data_nascimento') {
                    _form.find('input[name="' + v + '"]').val(dataBr(data[v]));
                } else if (v == 'sexo') {
                    _form.find('input[name="sexo"]').removeAttr('checked');
                    _form.find('input[name="sexo"][value="' + data[v] + '"]').attr('checked', 'checked').trigger('click');
                } else {
                    _form.find('input[name="' + v + '"]').val(data[v]);
                }
            });
            _form.modal('show');
            _form.find('button.salvar').unbind('click');
            _form.find('button.salvar').bind('click', salvarCandidato);
        }
    });


}

function salvarCandidato() {

    _form = $('#novo');
    id = _form.find('input[name="id"]').val();
    _obrigatorios = ['nome', 'email', 'data_nascimento', 'sexo'];
    erros = [];
    _obj = {}
    $.each(_obrigatorios, function (k, v) {
        if (_form.find('input[name="' + v + '"]').val() == '') {
            erros.push('Preencher o campo ' + v);
        } else {
            if (v == 'data_nascimento') {
                _obj[v] = formatDateDB(_form.find('input[name="' + v + '"]').val());
            } else {
                _obj[v] = _form.find('input[name="' + v + '"]').val();
            }
        }
    });
    _obj['sexo'] = _form.find('input[name="sexo"]:checked').val();
    console.log(JSON.stringify(_obj));

    if (erros.length > 0) {
        alert('ERRO: \n' + erros.join('\n'));
    } else {
        _type = (id != '') ? 'PUT' : 'POST';
        _url = (id != '') ? '../public/candidato/' + id : '../public/candidato';
        _form.find('input').attr('readonly', 'readonly');
        $.ajax({
            url: _url,
            headers: {"Content-type": 'application/json'},
            type: _type,
            data: JSON.stringify(_obj),
            success: function (data) {
                $('#novo').find('.close').trigger('click');
                carregaLista();
            }
        });
    }
}

function listaEventButtons() {
    o = $(this);
    if (o.hasClass('delete')) {
        if (confirm('Deseja mesmo remover o registro?')) {
            o.parents('tr').fadeOut(400, function () {
                $.ajax({
                    url: '../public/candidato/' + o.parents('tr').attr('idRegistro'),
                    headers: {"Content-type": 'application/json'},
                    type: 'DELETE',
                    success: function (data) {
                        console.log(data);
                        $(this).remove();
                    }
                });

            });
        }
    } else if (o.hasClass('edit')) {



    }
}
