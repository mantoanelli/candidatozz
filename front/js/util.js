function float2moeda(num) {
    x = 0;
    if (num < 0) {
        num = Math.abs(num);
        x = 1;
    }
    if (isNaN(num))
        num = "0";
    cents = Math.floor((num * 100 + 0.5) % 100);

    num = Math.floor((num * 100 + 0.5) / 100).toString();

    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.'
                + num.substring(num.length - (4 * i + 3));
    ret = num + ',' + cents;

    if (x == 1)
        ret = '-' + ret;
    return ret;
}

function moeda2float(moeda) {
    if (moeda) {
        if (moeda.indexOf('-') != -1) {
            sinal = '-';
            moeda = moeda.replace("-", "");
        } else {
            sinal = '';
        }
        moeda = moeda.replace(".", "");
        moeda = moeda.replace(".", "");
        moeda = moeda.replace(".", "");
        moeda = moeda.replace(".", "");
        moeda = moeda.replace(",", ".");
        moeda = parseFloat(moeda);
        moeda = new String(moeda);
        if (moeda.indexOf('.') == -1) {
            moeda = moeda + '.00';
        } else {
            decimal = moeda.substr(moeda.indexOf('.') + 1, 2);
            if (decimal.length < 2) {
                moeda = moeda + "0";
            }
        }
        moeda = sinal + moeda;
    }

    return new Number(moeda);
}

function formatDateJS(str) {
    arr = str.split(" ");
    dataArr = arr[0].split('/');
    data = dataArr[1] + '/' + dataArr[0] + '/' + dataArr[2];
    hora = "";
    if (arr[1]) {
        hora = arr[1];
    }
    r = new Date(data + " " + hora);
    return r;
}

function formatDateDB(str) {
    arr = str.split(" ");
    dataArr = arr[0].split('/');
    data = dataArr[2] + '-' + dataArr[1] + '-' + dataArr[0];
    hora = "";
    r = data;
    if (arr[1]) {
        hora = arr[1];
        r = data + " " + hora;
    }

    return r;
}

function dataBr(str, semhora) {
    arr = str.split(" ");
    dataArr = arr[0].split('-');
    data = dataArr[2] + '/' + dataArr[1] + '/' + dataArr[0];
    hora = "";
    r = data;
    if (arr[1] & !semhora) {
        hora = arr[1];
        r = data + " " + hora;
    }

    return r;
}