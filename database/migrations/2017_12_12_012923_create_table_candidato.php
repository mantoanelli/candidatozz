<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCandidato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cadidato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome',100);
            $table->string('email',250);
            $table->date('data_nascimento');
            $table->char('sexo',1);
            $table->dateTime('data_criado');
            $table->dateTime('data_modificado');
            $table->tinyInteger('ativo');
            $table->tinyInteger('deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cadidato');
    }
}
