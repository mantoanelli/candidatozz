<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});


$app->put('/candidato/{id}','CandidatoController@updateCandidato');
$app->delete('/candidato/{id}','CandidatoController@deleteCandidato');
$app->get('/candidato/','CandidatoController@index');
$app->get('/candidato/{id}','CandidatoController@index');

$app->post('/candidato','CandidatoController@createCandidato');